                <!-- Header -->
                    <header id="header" class="<!-- tmpl_var name='header-class' -->">
                        <h1><a href="index.html">LemonLDAP::NG</a></h1>
                        <nav id="nav">
                            <ul>
                                <li class="special">
                                    <a href="#menu" class="menuToggle"><span>Menu</span></a>
                                    <div id="menu">
                                        <ul>
                                            <li><a href="index.html">Home</a></li>
                                            <li><a href="download.html">Download</a></li>
                                            <li><a href="https://lemonldap-ng.org/documentation/latest/" target="_blank">Documentation</a></li>
                                            <li><a href="screenshots.html">Screenshots</a></li>
                                            <li><a href="references.html">References</a></li>
                                            <li><a href="contact.html">Contact</a></li>
                                            <li><a href="team.html">The team</a></li>
                                            <li><a href="conferences.html">Conferences</a></li>
                                            <li><a href="press-blog.html">Press and Blogs</a></li>
                                            <li><a href="sponsors.html">Sponsors</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </header>
