# Landing page for LL::NG website

Base theme from https://templated.co/transit

Site done with [Templer](https://github.com/skx/templer)

To build static files:
```
templer -f
```
