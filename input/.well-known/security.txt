-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Contact: mailto:lemonldap-ng-security@ow2.org
Expires: 2030-12-31T22:30:00.000Z
Encryption: https://lemonldap-ng.org/security/GPG-KEY-LLNG-SECURITY.asc
Preferred-Languages: en, fr
Canonical: https://www.lemonldap-ng.org/.well-known/security.txt
-----BEGIN PGP SIGNATURE-----

iJAEAREIADgWIQTfX0+/ddF4CvHSa6x7D6HyqVf6SAUCZwUvQhocc2VjdXJpdHlA
bGVtb25sZGFwLW5nLm9yZwAKCRB7D6HyqVf6SAIPAPsGosD9QyTDHvGVcB7Kc192
FidCjefllDOlxpT6Lj22QAD/b/83KIymhnmdIDBcBAAAY4NEAtFexU9hufRPohRN
gXQ=
=Z69U
-----END PGP SIGNATURE-----
