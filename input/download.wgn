title: Download
version: 2.20.2
version-: 2-20-2
----
                <!-- Main -->
                    <article id="main">

                        <header>
                            <h2><!-- tmpl_var name="title" --></h2>
                            <p>Version <!-- tmpl_var name="version" --></p>
                        </header>

                        <section class="wrapper style1">
                            <div class="inner">
                                <h3>Notes</h3>
                                <ul>
                                    <li><a href="https://projects.ow2.org/view/lemonldap-ng/lemonldap-ng-<!-- tmpl_var name="version-" -->-is-out/" target="_blank">Release notes</a></li>
                                    <li><a href="https://lemonldap-ng.org/documentation/latest/upgrade.html#id1" target="_blank">Upgrade notes</a></li>
                                </ul>
                                <p><em><a href="https://projects.ow2.org/bin/view/lemonldap-ng/" target="_blank">Read all release notes</a></em></p>
                            </div>
                        </section>

                        <section class="wrapper style2">
                            <div class="inner">
                                <h3>Packages, archives and more</h3>
                                <h4>RPM</h4>
                                <p>We recommend to install from our <a href="https://lemonldap-ng.org/documentation/latest/installrpm#yum-repository" target ="_blank">official yum repository</a>.</p>
                                <ul>
                                    <li><a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->_el7.rpm.tar.gz">RHEL/CentOS 7</a></li>
                                    <li><a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->_el8.rpm.tar.gz">RHEL/CentOS 8</a></li>
                                    <li><a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->_el9.rpm.tar.gz">RHEL/CentOS 9</a></li>
                                </ul>
                                <h4>Debian</h4>
                                <p>We recommend to install from our <a href="https://lemonldap-ng.org/documentation/latest/installdeb#ll-ng-repository" target ="_blank">official apt repository</a>.</p>
                                <ul>
                                    <li><a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->_deb.tar.gz">Debian stable</a></li>
                                </ul>
                                <h4>Docker</h4>
                                <pre><code>docker pull coudot/lemonldap-ng</code></pre>
                                <p>See <a href="https://lemonldap-ng.org/documentation/latest/docker.html" target="_blank">Docker documentation</a></p>
                                <h4>Sources</h4>
                                <p>Follow <a href="https://lemonldap-ng.org/documentation/latest/installtarball.html" target="_blank">documentation</a> to install form sources.</p>
                                <ul>
                                    <li><a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->.tar.gz">Tarball</a> (<a href="https://release.ow2.org/lemonldap/lemonldap-ng-<!-- tmpl_var name="version" -->).tar.gz.sig">GPG signature</a>)</li>
                                </ul>
                                <h4>Long Term Support (LTS)</h4>
                                <ul>
                                    <li>2.16 LTS:
                                    <ul>
                                        <li><a href="https://releases.ow2.org/lemonldap/lemonldap-ng-2.16.3.tar.gz">2.16.3 (tar.gz)</a></li>
                                        <li><a href="https://releases.ow2.org/lemonldap/lemonldap-ng-2.16.3_deb.tar.gz">2.16.3 (Debian)</a></li>
                                        <li><a href="https://releases.ow2.org/lemonldap/lemonldap-ng-2.16.3_el7.rpm.tar.gz">2.16.3 (EL7)</a></li>
                                        <li><a href="https://releases.ow2.org/lemonldap/lemonldap-ng-2.16.3_el8.rpm.tar.gz">2.16.3 (EL8)</a></li>
                                        <li><a href="https://releases.ow2.org/lemonldap/lemonldap-ng-2.16.3-1.el9.src.rpm">2.16.3 (EL9)</a></li>
                                    </ul>
                                    </li>
                                </ul>
                                <h4>Nigthly builds</h4>
                                <p><a href="http://lemonldap-ng.ow2.io/lemonldap-ng/" target="_blank">Gitlab PPA</a> with Debian/Ubuntu packages (master branch).</p>
                                <h4>Older versions</h4>
                                <p><a href="https://release.ow2.org/lemonldap/" target="_blank">Browse all versions</a>.</p>
                            </div>
                        </section>

                        <section class="wrapper style3">
                            <div class="inner">
                                <h3>Contributions</h3>
                                <ul>
                                    <li><a href="https://github.com/LemonLDAPNG" target="_blank">LemonLDAP::NG organization on Github</a></li>
                                    <li><a href="https://gitlab.ow2.org/lemonldap-ng" target="_blank">LemonLDAP::NG organization on OW2 Gitlab</a></li>
                                </ul>
                            </div>
                        </section>

                    </article>
